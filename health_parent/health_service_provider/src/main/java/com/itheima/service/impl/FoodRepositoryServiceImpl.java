package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.FoodRepositoryDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Food;
import com.itheima.service.FoodRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author nebula  k_nebula@163.com
 * @version 1.0
 * @date 2021-04-20 16:46
 */
@Service(interfaceClass = FoodRepositoryService.class)
@Transactional
public class FoodRepositoryServiceImpl implements FoodRepositoryService {

    @Autowired
    private FoodRepositoryDao foodRepositoryDao;

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();//查询条件
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        //select * from t_checkitem limit 0,10
        Page<CheckItem> page = foodRepositoryDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<CheckItem> rows = page.getResult();
        return new PageResult(total,rows);
    }

    @Override
    public void add(Food food) {
        foodRepositoryDao.add(food);
    }

    @Override
    public void edit(Food food) {
        foodRepositoryDao.edit(food);
    }
}
