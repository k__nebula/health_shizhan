package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.*;
import com.itheima.pojo.*;
import com.itheima.service.ReportService;
import com.itheima.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 运营数据统计服务
 */
@Service(interfaceClass = ReportService.class)
@Transactional
public class ReportServiceImpl implements ReportService {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private SetmealDao setmealDao;
    @Autowired
    private CheckGroupDao checkGroupDao;
    @Autowired
    private CheckItemDao checkItemDao;

    //查询运营数据
    public Map<String, Object> getBusinessReportData() throws Exception {
        //报表日期
        String today = DateUtils.parseDate2String(DateUtils.getToday());
        //获得本周一日期
        String thisWeekMonday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
        //获得本月第一天日期
        String firstDay4ThisMonth = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());

        //本日新增会员数
        Integer todayNewMember = memberDao.findMemberCountByDate(today);
        //总会员数
        Integer totalMember = memberDao.findMemberTotalCount();
        //本周新增会员数
        Integer thisWeekNewMember = memberDao.findMemberCountAfterDate(thisWeekMonday);
        //本月新增会员数
        Integer thisMonthNewMember = memberDao.findMemberCountAfterDate(firstDay4ThisMonth);

        //今日预约数
        Integer todayOrderNumber = orderDao.findOrderCountByDate(today);
        //本周预约数
        Integer thisWeekOrderNumber = orderDao.findOrderCountAfterDate(thisWeekMonday);
        //本月预约数
        Integer thisMonthOrderNumber = orderDao.findOrderCountAfterDate(firstDay4ThisMonth);
        //今日到诊数
        Integer todayVisitsNumber = orderDao.findVisitsCountByDate(today);
        //本周到诊数
        Integer thisWeekVisitsNumber = orderDao.findVisitsCountAfterDate(thisWeekMonday);
        //本月到诊数
        Integer thisMonthVisitsNumber = orderDao.findVisitsCountAfterDate(firstDay4ThisMonth);

        //热门套餐查询
        List<Map> hotSetmeal = orderDao.findHotSetmeal();

        Map<String, Object> result = new HashMap<>();
        result.put("reportDate", today);
        result.put("todayNewMember", todayNewMember);
        result.put("totalMember", totalMember);
        result.put("thisWeekNewMember", thisWeekNewMember);
        result.put("thisMonthNewMember", thisMonthNewMember);
        result.put("todayOrderNumber", todayOrderNumber);
        result.put("thisWeekOrderNumber", thisWeekOrderNumber);
        result.put("thisMonthOrderNumber", thisMonthOrderNumber);
        result.put("todayVisitsNumber", todayVisitsNumber);
        result.put("thisWeekVisitsNumber", thisWeekVisitsNumber);
        result.put("thisMonthVisitsNumber", thisMonthVisitsNumber);
        result.put("hotSetmeal", hotSetmeal);
        return result;
    }

    //查询会员数据
    public List<Map<String, String>> getexportMemberReport(List<Integer> ids) throws Exception {

        ArrayList<Map<String, String>> listValue = new ArrayList<>();
        for (Integer id : ids) {

            //获取该用户全部套餐
            List<Map<String, String>> listAll = orderDao.findByMemberId(id);

            for (Map<String, String> map : listAll) {
                HashMap<String, String> mapValue = new HashMap<>();
                //用户名
                String member = map.get("member");
                //性别
                String sex = map.get("sex");
                //手机号
                String phoneNumber = map.get("phoneNumber");
                //机构地址
                String healthAddress = map.get("healthAddress");

                //获取用户预约过的每个套餐
                String ssss = map.get("setmealId");
                System.out.println(ssss);
                Integer setmealId = Integer.valueOf(map.get("setmealId"));
                Setmeal setmeal = setmealDao.findById(setmealId);

                //套餐名
                String setmealName = setmeal.getName();

                //检查组信息
                StringBuilder checkGroupName = new StringBuilder();
                //检查项信息
                StringBuilder checkItemName = new StringBuilder();
                List<CheckGroup> checkGroups = setmeal.getCheckGroups();
                checkGroupName.append("[");
                checkItemName.append("[");
                for (CheckGroup group : checkGroups) {
                    checkGroupName.append(group.getName()).append(",");

                    List<CheckItem> checkItems = group.getCheckItems();
                    for (CheckItem checkItem : checkItems) {

                        checkItemName.append(checkItem.getName()).append(",");

                    }

                    checkItemName.append("]");
                }
                checkGroupName.append("]");


                mapValue.put("member", member);
                mapValue.put("sex", sex);
                mapValue.put("phoneNumber", phoneNumber);
                mapValue.put("healthAddress", healthAddress);
                mapValue.put("setmealName", setmealName);
                mapValue.put("checkGroupName", checkGroupName.toString());
                mapValue.put("checkItemName", checkItemName.toString());

                listValue.add(mapValue);
            }
        }
        return listValue;

    }
}
