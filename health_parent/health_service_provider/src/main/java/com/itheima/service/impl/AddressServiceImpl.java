package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.AddressDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Address;
import com.itheima.pojo.CheckItem;
import com.itheima.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = AddressService.class)
@Transactional
public class AddressServiceImpl implements AddressService {
    @Autowired
    private AddressDao addressDao;
    @Override
    public void add(Address address) {
        String healthImg = address.getHealthImg();
        System.out.println(healthImg);
        addressDao.add(address);
    }

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();//查询条件
        //完成分页查询，基于mybatis框架提供的分页助手插件完成
        PageHelper.startPage(currentPage,pageSize);
        //select * from t_checkitem limit 0,10
        Page<Address> page = addressDao.selectByCondition(queryString);
        long total = page.getTotal();
        List<Address> rows = page.getResult();
        return new PageResult(total,rows);
    }

    @Override
    public void delete(Integer id) {
        addressDao.delete(id);
    }

    @Override
    public Address findById(Integer id) {
        return addressDao.findById(id);
    }

    @Override
    public void edit(Address address) {
        addressDao.edit(address);
    }

    @Override
    public List<Map> findAll() {
        List<Address> addressList = addressDao.findAll();
        List<Map> list = new ArrayList<>();
        for (Address address : addressList) {
            Map<String, String> map = new HashMap<>();
            Integer id = address.getId();
            String healthName = address.getHealthName();
            String healthImg = address.getHealthImg();
            map.put("value", healthImg);
            map.put("label", healthName);
            list.add(map);
        }
        return list;
    }
}
