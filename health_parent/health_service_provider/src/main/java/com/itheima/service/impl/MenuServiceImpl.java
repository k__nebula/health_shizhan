package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.MenuDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;
import com.itheima.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service(interfaceClass = MenuService.class)
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuDao menuDao;

    @Override
    public List<Menu> findAll(Integer id) {
        return menuDao.findAll(id);
    }

    @Override
    public void add(Menu menu) {
        menuDao.add(menu);
    }

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        if (queryString!=null){
            queryString = queryString.trim();
        }
        PageHelper.startPage(currentPage,pageSize);
        Page<Menu> page = menuDao.findByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public Menu findById(Integer id) {
        return menuDao.findById(id);
    }

    @Override
    public void edit(Menu menu) {
            menuDao.update(menu);
    }

    @Override
    public void deleteById(Integer id) {

        //查询是否已关联角色t_role_menu；
        Integer count = menuDao.countByMenuId(id);
        if (count > 0){
            throw new RuntimeException();
        }
        menuDao.deleteById(id);
    }
    @Override
    public List<Menu> getMenuList(String username) {
        return menuDao.getMenuList(username);
    }
}
