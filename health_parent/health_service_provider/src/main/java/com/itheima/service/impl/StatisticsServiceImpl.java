package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.StatisticsServiceDao;
import com.itheima.entity.Result;
import com.itheima.pojo.ArrivalDto;
import com.itheima.pojo.DateReq;
import com.itheima.pojo.PhysicalDto;
import com.itheima.service.SetmealService;
import com.itheima.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 后管数据统计
 */
@Service(interfaceClass = StatisticsService.class)
public class StatisticsServiceImpl implements StatisticsService {

    private final StatisticsServiceDao statisticsServiceDao;

    public StatisticsServiceImpl(StatisticsServiceDao statisticsServiceDao) {
        this.statisticsServiceDao = statisticsServiceDao;
    }

    /*
        每日预约、到诊数量统计
     */
    @Override
    public Result findArrivalStatistics(DateReq dateReq) {
        try {
            //查询所有预约日期
            List<String> timeList = statisticsServiceDao.findArrivalDate(dateReq);
            //查询每天预约数量
            List<String> appointmentCount = statisticsServiceDao.findAppointmentCount(dateReq);
            //查询到诊数量
            List<String> arrivalCount = statisticsServiceDao.findArrivalCount(dateReq);
            ArrivalDto arrivalDto = new ArrivalDto();
            arrivalDto.setTimeList(timeList);
            arrivalDto.setAppointmentCount(appointmentCount);
            arrivalDto.setArrivalCoun(arrivalCount);
            return new Result(true, MessageConstant.QUERY_ORDER_SUCCESS,arrivalDto);
        } catch (Exception e) {
            e.fillInStackTrace();
            return new Result(false,MessageConstant.QUERY_ORDER_FAIL);
        }

    }


    /*
        体检收入统计
     */
    @Override
    public Result findPhysicalStatistics(DateReq dateReq) {
        try {
            //查询各个套餐收入
            List<PhysicalDto> list = statisticsServiceDao.findPhysicalStatistics(dateReq);
            PhysicalDto physicalDto = new PhysicalDto();
            physicalDto.setName("总收入");
            //查询总收入
            String totalCount = statisticsServiceDao.selectTotalCount(dateReq);
            physicalDto.setValue(totalCount);
            list.add(physicalDto);
            return new Result(true,MessageConstant.QUERY_SETMEAL_SUCCESS,list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_SETMEAL_FAIL);
        }
    }
}
