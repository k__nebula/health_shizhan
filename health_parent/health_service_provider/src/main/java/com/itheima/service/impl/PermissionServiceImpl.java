package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.PermissionService;
import com.itheima.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Set;

/**
 * 用户服务
 */
@Service(interfaceClass = PermissionService.class)
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;


    @Override
    public void deleteById(Integer id) {

        Integer num = permissionDao.queryRoleCountByPId(id);
        if (num>0){
            throw new RuntimeException();
        }

        permissionDao.deleteById(id);
    }

    @Override
    public void edit(Permission permission) {

        permissionDao.edit(permission);
    }

    @Override
    public Permission  findById(Integer id) {
        return permissionDao.findById(id);
    }

    @Override
    public void add(Permission permission) {
        permission.setCreatetime(new Date());
        permissionDao.add(permission);
    }

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        if (queryString!=null){
            queryString = queryString.trim();
        }
        PageHelper.startPage(currentPage,pageSize);
        Page<Permission> page = permissionDao.findByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());

    }
}
