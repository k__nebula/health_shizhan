package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.RoleDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = RoleService.class)
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;


    @Override
    public Result findAll() {
        List<Role> list = roleDao.findAll();
        return new Result(true, MessageConstant.QUERY_ROLE_SUCCESS,list);
    }

    @Override
    public PageResult selectAllByName(QueryPageBean queryPageBean) {
        //获取查询参数
        String queryString = queryPageBean.getQueryString();
        //使用分页插件
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<Role> rolePage = roleDao.selectAllByName(queryString);
        return new PageResult(rolePage.getTotal(),rolePage.getResult());
    }

    @Override
    public Result update(Integer[] permissionIds, Role role, Integer[] menuIds) {
        try {
            //修改角色表
            roleDao.update(role);
            //修改对应权限表id  先删除 在添加
            roleDao.deletePermission(role.getId());
            for (Integer permissionId : permissionIds) {
                roleDao.add(permissionId,role.getId());
            }
            //修改对应菜单表id
            roleDao.deleteMenu(role.getId());
            for (Integer menuId : menuIds) {
                roleDao.addMenu(menuId,role.getId());
            }
            return new Result(true,"编辑成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"编辑失败");
        }
    }

    @Override
    public Result add(Integer[] permissionIds, Role role, Integer[] menuIds) {
        try {
            //添加角色表
            roleDao.addRole(role);
            //添加该角色对应权限
            for (Integer permissionId : permissionIds) {
                roleDao.add(permissionId,role.getId());
            }
            //添加该角色对应菜单
            for (Integer menuId : menuIds) {
                roleDao.addMenu(menuId,role.getId());
            }
            return new Result(true,"添加角色信息成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"添加角色信息失败");
        }
    }

    @Override
    public Result findAllPermission() {
        try {
            List<Permission> permissionList = roleDao.findAllPermission();
            return new Result(true,"获取权限信息成功",permissionList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"获取权限信息失败");
        }
    }

    @Override
    public Result findAllMenu() {
        try {
            //查询所有菜单
            List<Menu> menuList = roleDao.findAllMenu();
            return new Result(true,"查询所有菜单成功",menuList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"查询所有菜单失败");
        }
    }

    @Override
    public Result deleteRole(int id) {
        try {
            //删除角色
            roleDao.deleteRole(id);
            return new Result(true,"删除角色成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除角色失败,你的角色有关联的权限或菜单未删除");
        }
    }
}
