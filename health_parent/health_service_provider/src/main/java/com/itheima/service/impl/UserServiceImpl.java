package com.itheima.service.impl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * 用户服务
 */
@Service(interfaceClass = UserService.class)
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PermissionDao permissionDao;
    //根据用户名查询数据库获取用户信息和关联的角色信息，同时需要查询角色关联的权限信息
    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);//查询用户基本信息，不包含用户的角色
        if(user == null){
            return null;
        }
        Integer userId = user.getId();
        //根据用户ID查询对应的角色
        Set<Role> roles = roleDao.findByUserId(userId);
        for (Role role : roles) {
            Integer roleId = role.getId();
            //根据角色ID查询关联的权限
            Set<Permission> permissions = permissionDao.findByRoleId(roleId);
            role.setPermissions(permissions);//让角色关联权限
        }
        user.setRoles(roles);//让用户关联角色
        return user;
    }

    @Override
    public List<Integer> findroleIdsByUserId(Integer id) {
        return userDao.findroleIdsByUserId(id);
    }

    @Override
    public void deleteById(Integer id) {
        userDao.deleteById(id);
        //同时删除该用户对应的角色信息；
        //代码略。。。
    }

    @Override
    public void edit(User user,Integer[] roleIds) {
        String password = user.getPassword();
        if (StringUtils.isBlank(password))
            password = new BCryptPasswordEncoder().encode(password);
        user.setPassword(password);
         userDao.edit(user);
         //编辑对应的角色信息；先删除再插入；
        Integer id = user.getId();
        userDao.deleteRolesByUserid(id);
        if(roleIds!=null && roleIds.length>0) {
            userDao.addUserRole(id, roleIds);
        }
    }

    @Override
    public User findById(Integer id) {
        return userDao.findById(id);
    }

     @Override
    public Integer countByName(String username) {
        return  userDao.countByName(username);
    }


    @Override
    public void add(User user,Integer[] roleIds) {


        String password = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(password);

        userDao.add(user);
        //编辑对应的角色信息；直接插入；
        if(roleIds!=null && roleIds.length>0) {
            Integer id = user.getId();
            userDao.addUserRole(id, roleIds);
        }
    }

    @Override
    public PageResult pageQuery(QueryPageBean queryPageBean) {
        Integer currentPage = queryPageBean.getCurrentPage();
        Integer pageSize = queryPageBean.getPageSize();
        String queryString = queryPageBean.getQueryString();
        if (queryString!=null){
            queryString = queryString.trim();
        }
        PageHelper.startPage(currentPage,pageSize);
        Page<User> page = userDao.findByCondition(queryString);
        return new PageResult(page.getTotal(),page.getResult());

    }
}
