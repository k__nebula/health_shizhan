package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Address;

import java.util.List;

public interface AddressDao {
    void add(Address address);
    Page<Address> selectByCondition(String queryString);
    void delete(Integer id);
    Address findById(Integer id);
    void edit(Address address);
    List<Address> findAll();
    String findImg(Integer id);
}
