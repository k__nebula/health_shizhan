package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Menu;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface MenuDao {

    @Select("<script> select * from t_menu where level = 1 " +
            "<if test=\"queryString!=null and queryString!=''\">" +
            " and  name like concat('%',#{queryString},'%') " +
            "</if> order by priority </script>")
    @Results(id = "results",value = {
            @Result(id = true,column = "id",property = "id"),
            @Result(column = "id",property = "children",
            many = @Many(select = "com.itheima.dao.MenuDao.findByParentId")
            )
    })
    Page<Menu> findByCondition(@Param("queryString") String queryString);

    @Select("select * from t_menu where parentMenuId = #{id} order by priority asc")
    @ResultMap("results")
    List<Menu> findByParentId(Integer id);

    //根据用户名动态查询菜单
    @Select("select m.* from t_menu m ,t_role_menu rm ,t_user_role ur,t_user u " +
            "where u.id = ur.user_id and ur.role_id = rm.role_id and rm.menu_id = m.id " +
            "and u.username = #{username} and level = 1 order by priority")
    @ResultMap("results")
    List<Menu> getMenuList(String username);


    @Select("<script>select * from t_menu " +
            "<if test='id!=null'>" +
            "where id != #{id} " +
            "</if>" +
            "</script>")
    List<Menu> findAll(Integer id);

    @Insert("insert into t_menu (name,linkUrl,path,priority,icon,description,parentMenuId,level) " +
            "values(#{name},#{linkUrl},#{path},#{priority},#{icon},#{description},#{parentMenuId},#{level})")
    void add(Menu menu);

    @Select("select * from t_menu where id = #{id}")
    Menu findById(Integer id);
    @Update("update t_menu set name = #{name},linkUrl = #{linkUrl},path = #{path},priority = #{priority}," +
            "icon = #{icon},description = #{description},parentMenuId = #{parentMenuId},level = #{level} " +
            "  where id = #{id}")
    void update(Menu menu);
    @Select("select count(role_id) as num from t_role_menu where menu_id = #{id}")
    Integer countByMenuId(Integer id);
    @Delete("delete from t_menu where id = #{id}")
    void deleteById(Integer id);
}






















