package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Permission;
import com.itheima.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.Set;

public interface PermissionDao {
    public Set<Permission> findByRoleId(Integer roleId);

    @Select("<script> select * from t_permission " +
            "<if test=\"queryString!=null and queryString!=''\">" +
            "where name like concat('%',#{queryString},'%') " +
            "</if> </script>")
    Page<Permission> findByCondition(@Param("queryString")String queryString);

    @Insert("insert into t_permission (name,keyword,description,station,createtime) " +
            "values(#{name},#{keyword},#{description},#{station},#{createtime})")
    void add(Permission permission);

    @Select("select * from t_permission where id = #{id}")
    Permission findById(Integer id);

    @Update("UPDATE t_permission " +
            "    set name = #{name},keyword = #{keyword},description = #{description},station = #{station}" +
            "    where id = #{id}")
    void edit(Permission permission);

    @Delete("delete from t_permission where id = #{id}")
    void deleteById(Integer id);

    @Select("select count(role_id) as num from t_role_permission where permission_id = #{id}")
    Integer queryRoleCountByPId(Integer id);
}
