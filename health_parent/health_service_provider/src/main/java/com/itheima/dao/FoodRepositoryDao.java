package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Food;

/**
 * @author nebula  k_nebula@163.com
 * @version 1.0
 * @date 2021-04-20 16:47
 */
public interface FoodRepositoryDao {

    Page<CheckItem> selectByCondition(String queryString);

    void add(Food food);

    public void edit(Food food);
}
