package com.itheima.dao;

import com.itheima.pojo.DateReq;
import com.itheima.pojo.PhysicalDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StatisticsServiceDao {

    /*
        查询所有预约日期
     */
    List<String> findArrivalDate(DateReq dateReq);

    /*
        查询到诊数量
     */
    List<String> findArrivalCount(DateReq dateReq);

    /*
        查询每天预约数量
     */
    List<String> findAppointmentCount(DateReq dateReq);

    /*
        查询各个套餐收入
     */
    List<PhysicalDto> findPhysicalStatistics(DateReq dateReq);

    /*
        查询总收入
    */
    String selectTotalCount(DateReq dateReq);
}
