package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Order;

import java.util.List;
import java.util.Map;

public interface OrderDao {
    public void add(Order order);
    public List<Order> findByCondition(Order order);
    public Map findById4Detail(Integer id);
    public Integer findOrderCountByDate(String date);
    public Integer findOrderCountAfterDate(String date);
    public Integer findVisitsCountByDate(String date);
    public Integer findVisitsCountAfterDate(String date);
    public List<Map> findHotSetmeal();
    public List<Map<String, String>> findByMemberId(Integer id);
    public List<Order> findByMemberId4Order(Integer id);
    public List<Order> findByMemberId1(Integer id);
    public Page<Map<String,Object>> findByCondition1(String queryString);
    public void edit(Map<String, Object> map);
    public void delete(Map<String, Object> map);
    public void updateStatusById(Map<String, Object> map);
}
