package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

public interface RoleDao {
    public Set<Role> findByUserId(Integer userId);

    @Select("select * from t_role")
    List<Role> findAll();

    Page<Role> selectAllByName(String queryString);

    Set<Permission> findPermission(int id);

    @Select("select * from t_menu m,t_role_menu rm where m.id = rm.menu_id and rm.role_id = #{id}")
    List<Menu> findByMenu(Integer id);

    void addRole(Role role);

    @Insert("insert into t_role_permission values(#{rid},#{pid})")
    void add(@Param("pid") Integer permissionId, @Param("rid") Integer id);

    @Insert("insert into t_role_menu values(#{rid},#{mid})")
    void addMenu(@Param("mid") Integer menuId,@Param("rid") Integer id);

    void update(Role role);

    @Delete("delete from t_role_permission where role_id = #{id}")
    void deletePermission(Integer id);

    @Delete("delete from t_role_menu where role_id = #{id}")
    void deleteMenu(Integer id);

    @Select("select * from t_permission")
    List<Permission> findAllPermission();

    @Select("select * from t_menu")
    List<Menu> findAllMenu();

    @Delete("DELETE FROM t_role WHERE id = #{id}")
    void deleteRole(int id);

}
