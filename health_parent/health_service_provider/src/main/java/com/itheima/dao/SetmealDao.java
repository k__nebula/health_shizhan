package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {
    public void add(Setmeal setmeal);
    public void setSetmealAndCheckGroup(Map map);
    public Page<Setmeal> findByCondition(String queryString);
    public List<Setmeal> findAll();
    public Setmeal findById(int id);
    public List<Map<String, Object>> findSetmealCount();
    public List<Integer> findCheckGroupBySetmealId(Integer setmealId);
    public List<CheckGroup> findCheckGroup(Integer id);
    public List<Map<String, String>> findSetmealNames();
}
