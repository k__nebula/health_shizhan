package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserDao {
    public User findByUsername(String username);

    @Select("<script> select id,username,birthday,gender,remark,station,telephone from t_user " +
            "<if test=\"queryString!=null and queryString!=''\">" +
            "where username like concat('%',#{queryString},'%') " +
            "</if> </script>")
    Page<User> findByCondition(@Param("queryString") String queryString);

    @Insert("insert into t_user (username,birthday,gender,password,remark,station,telephone) " +
            "values(#{username},#{birthday},#{gender},#{password},#{remark},#{station},#{telephone})")
    @Options(useGeneratedKeys = true,keyColumn = "id")
    void add(User user);

    @Select("select id,username,birthday,gender,remark,station,telephone from t_user where id = #{id}")
    User findById(Integer id);

    @Update("UPDATE t_user " +
            "    set username = #{username},birthday = #{birthday},gender = #{gender},password = #{password},remark = #{remark},station = #{station},telephone = #{telephone} " +
            "    where id = #{id}")
    void edit(User user);

    @Delete("delete from t_user where id = #{id}")
    void deleteById(Integer id);

     @Insert("<script>insert into t_user_role (user_id,role_id) values " +
            "<if test='roleIds!=null'>" +
            "<foreach collection=\"roleIds\" item=\"roleId\" separator=\",\">" +
            "            (#{userId},#{roleId})" +
            " </foreach>" +
            "</if>" +
            "</script>")
    void addUserRole(@Param("userId")Integer userId,@Param("roleIds") Integer[] roleIds);

    @Delete("delete from t_user_role where user_id = #{id} ")
    void deleteRolesByUserid(Integer id);
    @Select("select role_id from t_user_role where user_id = #{id}")
    List<Integer> findroleIdsByUserId(Integer id);

    @Select("select count(*) from t_user where username = #{username}")
    Integer countByName(String username);
}
