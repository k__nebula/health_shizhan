package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Member;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface MemberDao {
    public List<Member> findAll();
    public Page<Member> selectByCondition(String queryString);
    public void add(Member member);
    public void deleteById(Integer id);
    public Member findById(Integer id);
    public Member findByTelephone(String telephone);
    public void edit(Member member);
    public Integer findMemberCountBeforeDate(String date);
    public Integer findMemberCountByDate(String date);
    public Integer findMemberCountAfterDate(String date);
    public Integer findMemberTotalCount();

    public String findNameById(Integer id);
    public String findSexById(Integer id);
    public String findTelephoneById(Integer id);

    @Select("select o.orderDate ,m.name as membername ,s.name as setmealname,concat('https://zhonglixiu-1255313915.file.myqcloud.com/',img) img " +
            "from t_order o ,t_member m ,t_setmeal s " +
            "where o.member_id = m.id and s.id = o.setmeal_id and m.phoneNumber = #{telephone} " +
            "  and orderStatus = '已到诊'  order by orderDate desc limit 1")
    Map findOrderByTelephone(String telephone);
}
