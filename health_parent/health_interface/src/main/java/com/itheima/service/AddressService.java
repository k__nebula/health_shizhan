package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Address;

import java.util.List;
import java.util.Map;

public interface AddressService {
    void add(Address address);
    PageResult pageQuery(QueryPageBean queryPageBean);
    void delete(Integer id);
    Address findById(Integer id);
    void edit(Address address);
    List<Map> findAll();
}
