package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Member;

import java.util.List;
import java.util.Map;

public interface MemberService {
    public Member findByTelephone(String telephone);
    public List<Integer> findMemberCountByMonths(List<String> months);
    public void add(Member member);
    public PageResult pageQuery(QueryPageBean queryPageBean);
    public void deleteById(Integer id);
    public void edit(Member member);
    public Member findById(Integer id);
    public List<Member> findAll();

    Map findOrderByTelephone(String telephone);
}
