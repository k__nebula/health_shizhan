package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Order;

import java.util.List;
import java.util.Map;

public interface OrderService {
    public Result order(Map map) throws Exception;
    public Map findById(Integer id) throws Exception;
    public PageResult pageQuery(QueryPageBean queryPageBean);
    public Result add(Map<String,Object> map, Integer[] setmealIds);
    public void edit(Map<String,Object> map);
    public void delete(Map<String,Object> map);
    public void updateStatusById(Map<String,Object> map);
    public List fidByMemberId(String telephone);
}
