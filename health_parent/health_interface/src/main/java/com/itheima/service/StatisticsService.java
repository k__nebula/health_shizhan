package com.itheima.service;

import com.itheima.entity.Result;
import com.itheima.pojo.DateReq;

/**
 * 后管数据统计业务层接口
 */
public interface StatisticsService {


    /*
        每日预约、到诊数量统计
     */
    Result findArrivalStatistics(DateReq dateReq);

    /*
        体检收入统计
     */
    Result findPhysicalStatistics(DateReq dateReq);
}
