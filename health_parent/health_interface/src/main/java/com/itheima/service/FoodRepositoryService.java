package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Food;

/**
 * @author nebula  k_nebula@163.com
 * @version 1.0
 * @date 2021-04-20 16:44
 */
public interface FoodRepositoryService {

    public PageResult pageQuery(QueryPageBean queryPageBean);

    public void add(Food food);

    public void edit(Food food);
}
