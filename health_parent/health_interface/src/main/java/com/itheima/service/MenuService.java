package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;

import java.util.List;

public interface MenuService {


     void add(Menu menu) ;

    PageResult pageQuery(QueryPageBean queryPageBean);

    List<Menu> findAll(Integer id);

    Menu findById(Integer id);

    void edit(Menu menu);

    void deleteById(Integer id);

    List<Menu> getMenuList(String username);
}
