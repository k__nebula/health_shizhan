package com.itheima.service;

import java.util.List;
import java.util.Map;

public interface ReportService {
    public Map<String,Object> getBusinessReportData() throws Exception;
    public List<Map<String, String>> getexportMemberReport(List<Integer> ids) throws Exception;
}
