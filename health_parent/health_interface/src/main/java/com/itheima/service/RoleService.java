package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Role;

import java.util.List;

public interface RoleService {


    Result findAll();

    PageResult selectAllByName(QueryPageBean queryPageBean);

    Result update(Integer[] permissionIds, Role role,Integer[] menuIds);

    Result add(Integer[] permissionIds, Role role,Integer[] menuIds);

    Result findAllPermission();

    Result findAllMenu();

    Result deleteRole(int id);
}
