package com.itheima.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 封装查询条件
 */
public class QueryPageBean implements Serializable{
    private Integer currentPage;//页码
    private Integer pageSize;//每页记录数
    private String queryString;//查询条件

    /*startDate:this.dateFrame[0],//起始时间
    endDate:this.dateFrame[1],//结束时间
    typeOrStatus:this.typeAndStatus[1],//预约类型或预约状态
    setmealId:this.setmealName,
    nameOrPhoneNumber:this.nameOrPhoneNumber*/
    private Date startDate;
    private Date endDate;
    private String typeOrStatus;
    private String setmealId;
    private String nnameOrPhoneNumber;



    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTypeOrStatus() {
        return typeOrStatus;
    }

    public void setTypeOrStatus(String typeOrStatus) {
        this.typeOrStatus = typeOrStatus;
    }

    public String getSetmealId() {
        return setmealId;
    }

    public void setSetmealId(String setmealId) {
        this.setmealId = setmealId;
    }

    public String getNnameOrPhoneNumber() {
        return nnameOrPhoneNumber;
    }

    public void setNnameOrPhoneNumber(String nnameOrPhoneNumber) {
        this.nnameOrPhoneNumber = nnameOrPhoneNumber;
    }
}