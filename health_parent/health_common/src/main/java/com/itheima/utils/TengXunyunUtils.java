package com.itheima.utils;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * 腾讯云自建工具类
 */
public class TengXunyunUtils {
    //腾讯云秘钥，bucket
    public static String secretId1  = "AKIDBKFvKo7D8lBQPA460g5kPpM4QWTIlTu4";
    public static String secretKey1 = "YrugCTfbHo5IbdP1TlxyiCFCzWpZAB9C";
    public static String bucket1    = "ap-beijing";
    public static String buckName1  = "zhonglixiu-1255313915";

    public static void uploadTengXunYun(InputStream inputStream,String fileName) throws IOException {
        // 1 初始化用户身份信息（secretId, secretKey）。
        String secretId = secretId1;
        String secretKey = secretKey1;
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region("ap-beijing");
        ClientConfig clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // Bucket的命名格式为 BucketName-APPID ，此处填写的存储桶名称必须为此格式
        String bucketName = buckName1;
        // 命名要创建的目录，一定要以 '/' 结尾
        String key = fileName;
        // 创建一个大小为 500 的流
        ObjectMetadata metadata = new ObjectMetadata();
        // 设置输入流长度为500
        cosClient.putObject(bucketName, key, inputStream, null);
        cosClient.shutdown();

    }
    //删除文件
    public static void deleteFileFromTengXun(String fileName){
        // 1 初始化用户身份信息（secretId, secretKey）。
        String secretId = secretId1;
        String secretKey = secretKey1;
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置 bucket 的地域, COS 地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig 中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        Region region = new Region(bucket1);
        ClientConfig clientConfig = new ClientConfig(region);
        // 这里建议设置使用 https 协议
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // Bucket的命名格式为 BucketName-APPID ，此处填写的存储桶名称必须为此格式
        String bucketName = buckName1;
        String key = fileName;
        cosClient.deleteObject(bucketName, key);
        cosClient.shutdown();
    }
}
