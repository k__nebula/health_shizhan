package com.itheima.pojo;

import java.io.Serializable;

/**
 *  体检信息
 */
public class PhysicalDto implements Serializable {

    //套餐名
    private String name;
    //钱
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "PhysicalDto{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
