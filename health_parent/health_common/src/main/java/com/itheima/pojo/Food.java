package com.itheima.pojo;

import java.io.Serializable;

/**
 * @author nebula  k_nebula@163.com
 * @version 1.0
 * @date 2021-04-20 16:56
 */
public class Food implements Serializable {
    private Integer id;
    private String foodName;
    private String foodInfo;
    private String foodUnit;
    private String age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getFoodInfo() {
        return foodInfo;
    }

    public void setFoodInfo(String foodInfo) {
        this.foodInfo = foodInfo;
    }

    public String getFoodUnit() {
        return foodUnit;
    }

    public void setFoodUnit(String foodUnit) {
        this.foodUnit = foodUnit;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
