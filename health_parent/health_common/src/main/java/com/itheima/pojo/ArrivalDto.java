package com.itheima.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * 每日预约、到诊数量统计
 */
public class ArrivalDto implements Serializable {

    //所有预约时间
    private List<String> timeList;

    //每天预约人数
    private List<String> appointmentCount;

    //每天到诊数量
    private List<String> arrivalCoun;

    public List<String> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<String> timeList) {
        this.timeList = timeList;
    }

    public List<String> getAppointmentCount() {
        return appointmentCount;
    }

    public void setAppointmentCount(List<String> appointmentCount) {
        this.appointmentCount = appointmentCount;
    }

    public List<String> getArrivalCoun() {
        return arrivalCoun;
    }

    public void setArrivalCoun(List<String> arrivalCoun) {
        this.arrivalCoun = arrivalCoun;
    }


}
