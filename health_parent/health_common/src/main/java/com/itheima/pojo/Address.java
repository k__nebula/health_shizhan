package com.itheima.pojo;

import java.io.Serializable;

public class Address implements Serializable {
    private Integer id;
    private String healthName;
    private String healthPhone;
    private String healthLal;
    private String healthImg;
    private String healthAddress;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHealthName() {
        return healthName;
    }

    public void setHealthName(String healthName) {
        this.healthName = healthName;
    }

    public String getHealthPhone() {
        return healthPhone;
    }

    public void setHealthPhone(String healthPhone) {
        this.healthPhone = healthPhone;
    }

    public String getHealthLal() {
        return healthLal;
    }

    public void setHealthLal(String healthLal) {
        this.healthLal = healthLal;
    }

    public String getHealthImg() {
        return healthImg;
    }

    public void setHealthImg(String healthImg) {
        this.healthImg = healthImg;
    }

    public String getHealthAddress() {
        return healthAddress;
    }

    public void setHealthAddress(String healthAddress) {
        this.healthAddress = healthAddress;
    }
}
