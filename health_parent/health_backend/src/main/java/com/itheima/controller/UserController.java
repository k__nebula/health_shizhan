package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 检查组管理
 */

@RestController
@RequestMapping("/user")
public class UserController {
    @Reference
    private UserService userService;

    @GetMapping("/getUsername")
    public Result getUser(){
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user==null)
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        String userName= user.getUsername();
        //也可以直接查询并返回username
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return new Result(true, MessageConstant.GET_USERNAME_SUCCESS,username);
    }

    //新增检查组
    @RequestMapping("/add")
    public Result add(@RequestBody User user,Integer[] roleIds){
        try{
            Integer num  = userService.countByName(user.getUsername());
            if (num>0)
                return new Result(false, "用户名已存在");//新增失败
            userService.add(user,roleIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_USER_FAIL);//新增失败
        }
        return new Result(true,MessageConstant.ADD_USER_SUCCESS);//新增成功
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return userService.pageQuery(queryPageBean);
    }

    //根据ID查询检查组
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{
            User user = userService.findById(id);
            return new Result(true,MessageConstant.QUERY_USER_SUCCESS,user);//查询成功
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_USER_FAIL);//查询失败
        }
    }



    //编辑检查组
    @RequestMapping("/edit")
    public Result edit(@RequestBody User user,Integer[] roleIds){
        try{
            userService.edit(user,roleIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_USER_FAIL);//新增失败
        }
        return new Result(true,MessageConstant.EDIT_USER_SUCCESS);//新增成功
    }

    //删除检查项
//    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")//权限校验
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try{
            userService.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.DELETE_USER_FAIL);
        }
        return  new Result(true, MessageConstant.DELETE_USER_SUCCESS);
    }
    //根据检查组ID查询检查组包含的多个检查项ID
    @RequestMapping("/findroleIdsByUserId")
    public Result findroleIdsByUserId(Integer id){
        try{
            List<Integer> checkitemIds = userService.findroleIdsByUserId(id);
            return new Result(true,MessageConstant.QUERY_ROLE_SUCCESS,checkitemIds);//查询成功
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ROLE_FAIL);//查询失败
        }
    }

}
