package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Food;
import com.itheima.service.FoodRepositoryService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nebula  k_nebula@163.com
 * @version 1.0
 * @date 2021-04-20 16:38
 */
@RestController
@RequestMapping("/food")
public class FoodRepositoryController {

    @Reference
    private FoodRepositoryService foodRepositoryService;

    //膳食库分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = foodRepositoryService.pageQuery(queryPageBean);
        return pageResult;
    }

    //新增膳食库
    @RequestMapping("/add")
    public Result add(@RequestBody Food food){
        try{
            foodRepositoryService.add(food);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);
        }
        return  new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
    }

    //编辑检查项
    @RequestMapping("/edit")
    public Result edit(@RequestBody Food food){
        try{
            foodRepositoryService.edit(food);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.EDIT_CHECKITEM_FAIL);
        }
        return  new Result(true, MessageConstant.EDIT_CHECKITEM_SUCCESS);
    }
}
