package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Setmeal;
import com.itheima.service.OrderService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return orderService.pageQuery(queryPageBean);
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Map<String,Object> map, Integer[] setmealIds){
        try{
            return orderService.add(map,setmealIds);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_ORDER_FAIL);
        }
    }

    @RequestMapping("/edit")
    public Result edit(@RequestBody Map<String,Object> map){
        try {
            orderService.edit(map);
        } catch (Exception e) {
            return new Result(false,MessageConstant.EDIT_ORDER_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_ORDER_SUCCESS);
    }

    @RequestMapping("/delete")
    public Result delete(@RequestBody Map<String,Object> map){
        try {
            orderService.delete(map);
        } catch (Exception e) {
            return new Result(false,MessageConstant.DELETE_ORDER_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_ORDER_SUCCESS);
    }

    @RequestMapping("updateStatusById")
    public Result updateStatusById(@RequestBody Map<String,Object> map){
        try {
            orderService.updateStatusById(map);
        } catch (Exception e) {
            return new Result(false,MessageConstant.EDIT_ORDER_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_ORDER_SUCCESS);
    }

}
