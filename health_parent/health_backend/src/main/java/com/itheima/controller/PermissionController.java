package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Permission;
import com.itheima.pojo.User;
import com.itheima.service.PermissionService;
import com.itheima.service.UserService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 检查组管理
 */

@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Reference
    private PermissionService permissionService;
    //新增检查组
    @RequestMapping("/add")
    public Result add(@RequestBody Permission permission){
        try{
            permissionService.add(permission);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_PERMISSION_FAIL);//新增失败
        }
        return new Result(true,MessageConstant.ADD_PERMISSION_SUCCESS);//新增成功
    }

    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return permissionService.pageQuery(queryPageBean);
    }

    //根据ID查询检查组
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{
            Permission permission = permissionService.findById(id);
            return new Result(true,MessageConstant.QUERY_PERMISSION_SUCCESS,permission);//查询成功
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);//查询失败
        }
    }



    //编辑检查组
    @RequestMapping("/edit")
    public Result edit(@RequestBody Permission permission){
        try{
            permissionService.edit(permission);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_PERMISSION_FAIL);//新增失败
        }
        return new Result(true,MessageConstant.EDIT_PERMISSION_SUCCESS);//新增成功
    }

    //删除检查项
//    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")//权限校验
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try{
            permissionService.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.DELETE_PERMISSION_FAIL);
        }
        return  new Result(true, MessageConstant.DELETE_PERMISSION_SUCCESS);
    }


/*    //查询所有检查组
    @RequestMapping("/findAll")
    public Result findAll(){
        try{
            List<User> list = null;
//            userService.findAll();
            return new Result(true,MessageConstant.QUERY_USER_SUCCESS,list);//查询成功
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_USER_FAIL);//查询失败
        }
    }*/
}
