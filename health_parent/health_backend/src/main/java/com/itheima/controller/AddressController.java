package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Address;
import com.itheima.service.AddressService;
import com.itheima.utils.TengXunyunUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@RestController
@RequestMapping("/address")
public class AddressController {
    @Autowired
    private JedisPool jedisPool;
    @Reference
    private AddressService addressService;
    @RequestMapping("/add")
    public Result add(@RequestBody Address address){
        try {
            addressService.add(address);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_ADDRESS_FAIL);
        }
        return new Result(true, MessageConstant.ADD_ADDRESS_SUCCESS);
    }
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = addressService.pageQuery(queryPageBean);
        return pageResult;
    }
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try {
            addressService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_ADDRESS_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_ADDRESS_SUCCESS);
    }
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            Address address = addressService.findById(id);
            return  new Result(true, MessageConstant.QUERY_ADDRESS_SUCCESS,address);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ADDRESS_FAIL);
        }
    }
    //编辑检查项
    @RequestMapping("/edit")
    public Result edit(@RequestBody Address address){
        try{
            addressService.edit(address);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.EDIT_ADDRESS_FAIL);
        }
        return  new Result(true, MessageConstant.EDIT_ADDRESS_SUCCESS);
    }

    @RequestMapping("/upload")
    public Result upload(@RequestParam("imgFile") MultipartFile imgFile) {
        System.out.println(imgFile);
        String originalFilename = imgFile.getOriginalFilename();//原始文件名 3bd90d2c-4e82-42a1-a401-882c88b06a1a2.jpg
        int index = originalFilename.lastIndexOf(".");
        String extention = originalFilename.substring(index - 1);//.jpg
        String fileName = UUID.randomUUID().toString() + extention;//	FuM1Sa5TtL_ekLsdkYWcf5pyjKGu.jpg
        System.out.println("此处上传文件名为：" + fileName);
        try {
           /* //将文件上传到七牛云服务器
            QiniuUtils.upload2Qiniu(imgFile.getBytes(),fileName);*/
            //将文件上传到腾讯云服务器
            /**
             * 下面仔细整理腾讯云笔记，花了一天时间啊- -！
             */
            InputStream inputStream = imgFile.getInputStream();
            TengXunyunUtils.uploadTengXunYun(inputStream, fileName);
            jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES, fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.PIC_UPLOAD_FAIL);
        }
        return new Result(true, MessageConstant.PIC_UPLOAD_SUCCESS, fileName);
    }
}
