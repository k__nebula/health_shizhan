package com.itheima.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.DateReq;
import com.itheima.service.StatisticsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 后管数据统计
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    @Reference
    private StatisticsService statisticsService;

    /*
        每日预约、到诊数量统计
     */
    @RequestMapping("/findArrivalStatistics")
    public Result findArrivalStatistics(String startDate ,String endDate){
        DateReq dateReq = new DateReq();
        dateReq.setStartDate(startDate);
        dateReq.setEndDate(endDate);
        return statisticsService.findArrivalStatistics(dateReq);
    }

    /*
        体检收入统计
     */
    @RequestMapping("/findPhysicalStatistics")
    public Result findPhysicalStatistics(String startDate ,String endDate){
        DateReq dateReq = new DateReq();
        dateReq.setStartDate(startDate);
        dateReq.setEndDate(endDate);
        return statisticsService.findPhysicalStatistics(dateReq);
    }
}
