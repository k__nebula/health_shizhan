package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Reference
    private RoleService roleService;

    /*
    * 查询全部
    * */
    @RequestMapping("/findAll")
    public Result findAll(){
        return roleService.findAll();
    }

    /*
     * 角色分页查询
     * */
    @RequestMapping("/selectAllByName")
    public PageResult selectAllByName(@RequestBody QueryPageBean queryPageBean){
        return roleService.selectAllByName(queryPageBean);
    }
    /*
     * 角色编辑
     * */
    @RequestMapping("/update")
    public Result update(Integer[] permissionIds , @RequestBody Role role,Integer[] menuIds){
        return roleService.update(permissionIds,role,menuIds);
    }
    /*
     * 角色添加
     * */
    @RequestMapping("/add")
    public Result add(Integer[] permissionIds , @RequestBody Role role,Integer[] menuIds){
        return roleService.add(permissionIds,role,menuIds);
    }

    /*
     * 查询全部权限
     * */
    @RequestMapping("/findAllPermission")
    public Result findAllPermission(){
        return roleService.findAllPermission();
    }

    /*
     * 查询全部菜单
     * */
    @RequestMapping("/findAllMenu")
    public Result findAllMenu(){
        return roleService.findAllMenu();
    }

     /*
     * 删除角色
     * */
    @RequestMapping("/deleteRole")
    public Result deleteRole(int id){
        return roleService.deleteRole(id);
    }

}
