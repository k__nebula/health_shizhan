package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.service.MenuService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Reference
    private MenuService menuService;

    //获取动态菜单列表；
    @GetMapping("/getMenuList")
    public Result getMenuList(){
        //用户名唯一。
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Menu> list = menuService.getMenuList(username);

        return new Result(true, MessageConstant.GET_MENU_SUCCESS,list);
    }
    //分页查询
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        return menuService.pageQuery(queryPageBean);
    }
    //查询所有菜单列表
    @RequestMapping("/findAll")
    public Result findAll(@RequestParam(value = "id",required = false) int id){
        try{
            List<Menu> list =menuService.findAll(id);
            return new Result(true, MessageConstant.QUERY_MENU_SUCCESS,list);//查询成功
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MENU_FAIL);//查询失败
        }
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Menu menu){
        try{
            menuService.add(menu);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.ADD_MENU_FAIL);
        }
        return  new Result(true, MessageConstant.ADD_MENU_SUCCESS);
    }

    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{
            Menu menu = menuService.findById(id);
            return new Result(true,MessageConstant.QUERY_MENU_SUCCESS,menu);//查询成功
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_MENU_FAIL);//查询失败
        }
    }



    //编辑检查组
    @RequestMapping("/edit")
    public Result edit(@RequestBody Menu menu){
        try{
            menuService.edit(menu);
        }catch (Exception e){
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_MENU_FAIL);//新增失败
        }
        return new Result(true,MessageConstant.EDIT_MENU_SUCCESS);//新增成功
    }

    //删除检查项
//    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")//权限校验
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try{
            menuService.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
            //服务调用失败
            return new Result(false, MessageConstant.DELETE_MENU_FAIL);
        }
        return  new Result(true, MessageConstant.DELETE_MENU_SUCCESS);
    }

}
