package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.itheima.constant.MessageConstant;
import com.itheima.constant.RedisMessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import com.itheima.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 处理会员相关操作
 */

@RestController
@RequestMapping("/member")
public class MemberController {
    @Autowired
    private JedisPool jedisPool;

    @Reference
    private MemberService memberService;
    @Reference
    private OrderService orderService;

    //手机号快速登录
    @RequestMapping("/login")
    public Result login(HttpServletResponse response, @RequestBody Map map){
        String telephone = (String) map.get("telephone");
        String validateCode = (String) map.get("validateCode");
        //从Redis中获取保存的验证码
        String validateCodeInRedis = jedisPool.getResource().get(telephone + RedisMessageConstant.SENDTYPE_LOGIN);
        if(validateCodeInRedis != null && validateCode != null && validateCode.equals(validateCodeInRedis)){
            //验证码输入正确
            //判断当前用户是否为会员（查询会员表来确定）
            Member member = memberService.findByTelephone(telephone);
            if(member == null){
                //不是会员，自动完成注册（自动将当前用户信息保存到会员表）
                member = new Member();
                member.setRegTime(new Date());
                member.setPhoneNumber(telephone);
                memberService.add(member);
            }
            //向客户端浏览器写入Cookie，内容为手机号
            Cookie cookie = new Cookie("login_member_telephone",telephone);
            cookie.setPath("/");
            cookie.setMaxAge(60*60*24*30);
            response.addCookie(cookie);
            //将会员信息保存到Redis
            String json = JSON.toJSON(member).toString();
            jedisPool.getResource().setex(telephone,60*30,json);
            return new Result(true,MessageConstant.LOGIN_SUCCESS);
        }else{
            //验证码输入错误
            return new Result(false, MessageConstant.VALIDATECODE_ERROR);
        }
    }

    //用户个人信息回显
    @RequestMapping("/findById")
    public Result findById(HttpServletRequest request) {
        String telephone = null;
        //查询cookie并查询redis登陆状态
        try {
            telephone = this.checkLogin(request);
            if (telephone == null && telephone.equals("")) {
                return new Result(true, MessageConstant.GET_MEMBER_COOKIE_FAIL);
            }

            Member member = memberService.findByTelephone(telephone);
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, member);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        }
    }
    //用户个人信息回显
    @RequestMapping("/findByCookie")
    public Result findByCookie(HttpServletRequest request) {
        String telephone = null;
        //查询cookie并查询redis登陆状态
        try {
            telephone = this.checkLogin(request);
            if (telephone == null && telephone.equals("")) {
                return new Result(true, MessageConstant.GET_MEMBER_COOKIE_FAIL);
            }

            Map map = memberService.findOrderByTelephone(telephone);
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        }
    }

    @RequestMapping("/submit")
    public Result submit(@RequestBody Member member) {
        try {
            memberService.edit(member);
            return new Result(true, MessageConstant.EDIT_MEMBER_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_MEMBER_FAIL);
        }
    }

    @RequestMapping("/findorder")
    public Result findorder(HttpServletRequest request) {
        String telephone = null;
        //查询cookie并查询redis登陆状态
        try {
            telephone = this.checkLogin(request);
            if (telephone != null && !telephone.equals("")) {

                List list = orderService.fidByMemberId(telephone);

                return new Result(true, MessageConstant.GET_MEMBER_COOKIE_SUCCESS,list);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MEMBER_COOKIE_FAIL);
        }
        return new Result(false, MessageConstant.GET_MEMBER_COOKIE_FAIL);
    }




    public String checkLogin(HttpServletRequest request) {
        String telephone = null;
        Cookie[] cookies = request.getCookies();
        if (cookies.length == 0) {
            return null;
        }

        //从cookie中获取手机号
        for (Cookie cookie : cookies) {
            if ("login_member_telephone".equals(cookie.getName())) {
                telephone = cookie.getValue();
            }
        }
        String redisValue = null;
        //判断是否获取到手机号
        if (telephone != null && !telephone.equals("")) {
            //如果获取到 从redis中判断登陆状态
            redisValue = jedisPool.getResource().get(telephone);
        }
        //判断登陆状态 如果登陆则返回成功
        if (redisValue != null && !redisValue.equals("")) {
            return telephone;
        }
        return null;
    }
}
